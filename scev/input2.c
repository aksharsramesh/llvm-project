void foo(int a[], int n)
{
    int i;
    for (i = 0; i < n; i += 4)
    {
        a[i] = i;
        a[i + 3] = i + 3;
        a[i + 2] = i + 2;
        a[i + 1] = i + 1;
    }
}