; ModuleID = 'input.ll'
source_filename = "input.c"
target datalayout = "e-m:e-p270:32:32-p271:32:32-p272:64:64-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

; Function Attrs: noinline nounwind uwtable
define dso_local void @foo(i32* %a, i32 %n) #0 {
entry:
  %0 = sext i32 %n to i64
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %indvars.iv = phi i64 [ %indvars.iv.next, %for.inc ], [ 0, %entry ]
  %cmp = icmp slt i64 %indvars.iv, %0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %arrayidx = getelementptr inbounds i32, i32* %a, i64 %indvars.iv
  %1 = trunc i64 %indvars.iv to i32
  store i32 %1, i32* %arrayidx, align 4
  %2 = or i64 %indvars.iv, 1
  %3 = or i64 %indvars.iv, 1
  %arrayidx3 = getelementptr inbounds i32, i32* %a, i64 %3
  %4 = trunc i64 %2 to i32
  store i32 %4, i32* %arrayidx3, align 4
  %5 = or i64 %indvars.iv, 3
  %6 = or i64 %indvars.iv, 3
  %arrayidx7 = getelementptr inbounds i32, i32* %a, i64 %6
  %7 = trunc i64 %5 to i32
  store i32 %7, i32* %arrayidx7, align 4
  %8 = or i64 %indvars.iv, 2
  %9 = or i64 %indvars.iv, 2
  %arrayidx11 = getelementptr inbounds i32, i32* %a, i64 %9
  %10 = trunc i64 %8 to i32
  store i32 %10, i32* %arrayidx11, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %indvars.iv.next = add nuw nsw i64 %indvars.iv, 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

attributes #0 = { noinline nounwind uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="all" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (https://github.com/llvm/llvm-project.git 47ef09e4848a970c530928496b54085cfdba5a76)"}
