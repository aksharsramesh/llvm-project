#include "llvm/ADT/IndexedMap.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/Analysis/LoopPass.h"
#include "llvm/Analysis/ScalarEvolution.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/GlobalVariable.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Type.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Transforms/IPO/PassManagerBuilder.h"

using namespace llvm;

namespace
{
    struct hello : public FunctionPass
    {
        static char ID;
        hello() : FunctionPass(ID) {}
        bool runOnFunction(Function &F);

        std::map<Instruction *, const SCEV *> vecSCEV;
        std::vector<const SCEV *> difSCEV;
        ScalarEvolution *SE;

        void getAnalysisUsage(AnalysisUsage &AU) const override
        {
            AU.setPreservesAll();
            AU.addRequired<ScalarEvolutionWrapperPass>();
        }
    };
} // namespace

bool hello::runOnFunction(Function &F)
{

    SE = &getAnalysis<ScalarEvolutionWrapperPass>().getSE();
    for (Function::iterator bb = F.begin(); bb != F.end(); bb++)
    {
        for (BasicBlock::iterator I = bb->begin(); I != bb->end(); I++)
        {
            if (GEPOperator *PN = dyn_cast<GEPOperator>(I))
            {
                const SCEV *expr = SE->getSCEV(PN);
                vecSCEV[&*I] = expr;
            }
        }
        if (!vecSCEV.empty())
        {
            for (auto i = vecSCEV.begin(); i != prev(vecSCEV.end()); ++i)
            {
                unsigned Depth = 0;

                for (auto j = next(i); j != vecSCEV.end(); ++j)
                {
                    const SCEV *expr =
                        SE->getMinusSCEV(j->second, i->second, SCEV::FlagAnyWrap, Depth);
                    if (SE->isKnownNegative(&*expr))
                    {
                        for (User *U : (i->first)->users())
                        {
                            if (StoreInst *Inst = dyn_cast<StoreInst>(U))
                            {
                                for (User *U : (j->first)->users())
                                {
                                    if (StoreInst *InstNEW = dyn_cast<StoreInst>(U))
                                    {
                                        Inst->moveAfter(InstNEW);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            vecSCEV.clear();
        }
    }

    return false;
}

char hello::ID = 0;
static RegisterPass<hello> X("hello", "hello World Pass",
                             false /* Only looks at CFG */,
                             false /* Analysis Pass */);

static RegisterStandardPasses Y(PassManagerBuilder::EP_EarlyAsPossible,
                                [](const PassManagerBuilder &Builder,
                                   legacy::PassManagerBase &PM) {
                                    PM.add(new hello());
                                });
