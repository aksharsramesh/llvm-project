#include "llvm/ADT/APInt.h"
#include "llvm/ADT/IndexedMap.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/ADT/StringRef.h"
#include "llvm/Analysis/LoopPass.h"
#include "llvm/Analysis/ScalarEvolution.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/GlobalVariable.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Type.h"
#include "llvm/IR/User.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Transforms/IPO/PassManagerBuilder.h"

using namespace llvm;

namespace
{
  struct structField : public ModulePass
  {
    static char ID;
    structField() : ModulePass(ID) {}
    bool runOnModule(Module &M);

    std::map<uint64_t, uint64_t> vecSCEV;
    std::map<GetElementPtrInst *, uint64_t> GEPtoIDX;
    std::vector<uint32_t> deadFields;
    llvm::SmallVector<llvm::Value *, 2> idx_list;
    std::vector<Type *> NOTdeadFields;
    std::vector<Value *> CalledFunctions;
    uint64_t getIDX(GetElementPtrInst *GEP);

    void getAnalysisUsage(AnalysisUsage &AU) const override
    {
      AU.setPreservesAll();
      AU.addRequired<LoopInfoWrapperPass>();
      AU.addPreserved<LoopInfoWrapperPass>();
    }
  };
} // namespace

uint64_t structField::getIDX(GetElementPtrInst *GEP)
{
  uint64_t Idx;
  if (llvm::ConstantInt *CI =
          dyn_cast<llvm::ConstantInt>((GEP->idx_end() - 1)->get()))
  {
    Idx = CI->getZExtValue();
  }
  return Idx;
}

bool structField::runOnModule(Module &M)
{
  llvm::StructType *strct;
  llvm::AllocaInst *variable;

  for (Module::iterator F = M.begin(); F != M.end(); F++)
  {
    for (Function::iterator bb = F->begin(); bb != F->end(); bb++)
    {
      for (BasicBlock::iterator I = bb->begin(); I != bb->end(); I++)
      {
        if (GetElementPtrInst *PN = dyn_cast<GetElementPtrInst>(I))
        {
          if (strct = dyn_cast<llvm::StructType>(PN->getSourceElementType()))
          {
            uint64_t Idx = structField::getIDX(PN);
            LoopInfo &LoIn = getAnalysis<LoopInfoWrapperPass>(*F).getLoopInfo();
            uint64_t depth = LoIn.getLoopDepth(&*bb);
            if (vecSCEV.find(Idx) == vecSCEV.end())
            {
              vecSCEV[Idx] = pow(8, depth);
            }
            else
            {
              vecSCEV[Idx] += pow(8, depth);
            }
            GEPtoIDX[PN] = Idx;
          }
        }
        else if (CallInst *CI = dyn_cast<CallInst>(I))
        {
          if (Function *FN = dyn_cast<Function>(CI->getCalledOperand()))
          {
            if (!FN->isDeclaration())
              CalledFunctions.push_back(FN);
          }
        }
      }
    }
  }

  for (auto i = 0; i != strct->getNumElements(); i++)
  {
    if (vecSCEV.find(i) != vecSCEV.end())
    {
      if (vecSCEV[i] == 1)
      {
        deadFields.push_back(i);
      }
      else
      {
        NOTdeadFields.push_back(strct->getTypeAtIndex(i));
      }
    }
    else
    {
      deadFields.push_back(i);
    }
  }

  std::sort(deadFields.begin(), deadFields.end());

  for (auto j = GEPtoIDX.begin(); j != GEPtoIDX.end(); j++)
  {
    auto k = 0;
    for (auto i = deadFields.begin(); i != deadFields.end(); ++i)
    {
      if (j->second > *i)
      {
        k++;
      }
    }
    j->second -= k;
  }

  ConstantInt *Index =
      ConstantInt::get(llvm::Type::getInt32Ty(M.getContext()), 0);
  idx_list.push_back(Index);

  llvm::StructType *newStruct =
      llvm::StructType::create(NOTdeadFields, "newStruct", false);

  llvm::PointerType *pointer = llvm::PointerType::get(newStruct, NULL);

  for (Module::iterator F = M.begin(); F != M.end(); F++)
  {
    for (Function::iterator bb = F->begin(); bb != F->end(); bb++)
    {
      Instruction *InsertPt = (Instruction *)(bb->getFirstNonPHI());
      IRBuilder<> Builder(InsertPt);
      BasicBlock &EntryBlock = (F->getEntryBlock());
      if (&*bb == &EntryBlock)
      {
        variable = Builder.CreateAlloca(pointer, NULL, "name");
      }
      if (std::find(CalledFunctions.begin(), CalledFunctions.end(), &*F) !=
          CalledFunctions.end())
      {
        for (BasicBlock::iterator I = bb->begin(); I != bb->end(); I++)
        {
          if (GetElementPtrInst *PN = dyn_cast<GetElementPtrInst>(I))
          {
            if (GEPtoIDX.find(PN) != GEPtoIDX.end())
            {
              uint64_t Idx = structField::getIDX(PN);
              if (std::find(deadFields.begin(), deadFields.end(), Idx) ==
                  deadFields.end())
              {
                Index = ConstantInt::get(llvm::Type::getInt32Ty(M.getContext()),
                                         GEPtoIDX[PN]);
                idx_list.push_back(Index);
                Builder.SetInsertPoint(&*I);
                LoadInst *newLoad =
                    Builder.CreateLoad(pointer, variable, "newLoad");
                llvm::Value *newPN = Builder.CreateInBoundsGEP(
                    newStruct, newLoad, idx_list, PN->getName());
                PN->replaceAllUsesWith(newPN);
                idx_list.pop_back();
              }
              else
              {
                for (User *U : PN->users())
                {
                  if (StoreInst *Inst = dyn_cast<StoreInst>(U))
                  {
                    for (User *U : Inst->users())
                    {
                      if (StoreInst *InstNEW = dyn_cast<StoreInst>(U))
                      {
                        InstNEW->eraseFromParent();
                      }
                    }
                    Inst->eraseFromParent();
                  }
                }
              }
            }
          }
        }
      }
      else
      {
        for (BasicBlock::iterator I = bb->begin(); I != bb->end(); I++)
        {
          if (GetElementPtrInst *PN = dyn_cast<GetElementPtrInst>(I))
          {
            if (strct =
                    dyn_cast<llvm::StructType>(PN->getSourceElementType()))
            {
              for (User *U : PN->users())
              {
                if (StoreInst *Inst = dyn_cast<StoreInst>(U))
                {
                  for (User *U : Inst->users())
                  {
                    if (StoreInst *InstNEW = dyn_cast<StoreInst>(U))
                    {
                      InstNEW->eraseFromParent();
                    }
                  }
                  Inst->eraseFromParent();
                }
              }
            }
          }
        }
      }
    }
  }
  return true;
}

char structField::ID = 0;
static RegisterPass<structField> X("structField", "structField World Pass",
                                   false /* Only looks at CFG */,
                                   false /* Analysis Pass */);

static RegisterStandardPasses Y(PassManagerBuilder::EP_EarlyAsPossible,
                                [](const PassManagerBuilder &Builder,
                                   legacy::PassManagerBase &PM) {
                                  PM.add(new structField());
                                });
