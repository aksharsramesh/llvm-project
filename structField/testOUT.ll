; ModuleID = 'out.ll'
source_filename = "llvm-link"
target datalayout = "e-m:e-p270:32:32-p271:32:32-p272:64:64-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

%struct.ArrayInfo = type { i32, i32, double, i32, [200 x i32], i64, float*, [100 x i8], i64, i32 }

@arrayInfo = dso_local global %struct.ArrayInfo zeroinitializer, align 8
@.str = private unnamed_addr constant [15 x i8] c"LB %ld UB %ld\0A\00", align 1
@.str.1 = private unnamed_addr constant [11 x i8] c"Array is \0A\00", align 1
@.str.2 = private unnamed_addr constant [5 x i8] c" %f \00", align 1
@.str.3 = private unnamed_addr constant [14 x i8] c"\0A Sum is %lf\0A\00", align 1
@.str.4 = private unnamed_addr constant [3 x i8] c"%d\00", align 1

; Function Attrs: noinline nounwind optnone uwtable
define dso_local void @allocate(%struct.ArrayInfo* %info) #0 {
entry:
  %info.addr = alloca %struct.ArrayInfo*, align 8
  store %struct.ArrayInfo* %info, %struct.ArrayInfo** %info.addr, align 8
  %tmp = load %struct.ArrayInfo*, %struct.ArrayInfo** %info.addr, align 8
  %size = getelementptr inbounds %struct.ArrayInfo, %struct.ArrayInfo* %tmp, i32 0, i32 0
  %tmp1 = load i32, i32* %size, align 8
  %conv = sext i32 %tmp1 to i64
  %mul = mul i64 4, %conv
  %call = call noalias i8* @malloc(i64 %mul) #3
  %tmp2 = bitcast i8* %call to float*
  %tmp3 = load %struct.ArrayInfo*, %struct.ArrayInfo** %info.addr, align 8
  %array = getelementptr inbounds %struct.ArrayInfo, %struct.ArrayInfo* %tmp3, i32 0, i32 6
  store float* %tmp2, float** %array, align 8
  ret void
}

; Function Attrs: nounwind
declare dso_local noalias i8* @malloc(i64) #1

; Function Attrs: noinline nounwind optnone uwtable
define dso_local void @printLBUB(%struct.ArrayInfo* %info) #0 {
entry:
  %info.addr = alloca %struct.ArrayInfo*, align 8
  store %struct.ArrayInfo* %info, %struct.ArrayInfo** %info.addr, align 8
  %tmp = load %struct.ArrayInfo*, %struct.ArrayInfo** %info.addr, align 8
  %lb = getelementptr inbounds %struct.ArrayInfo, %struct.ArrayInfo* %tmp, i32 0, i32 5
  %tmp1 = load i64, i64* %lb, align 8
  %tmp2 = load %struct.ArrayInfo*, %struct.ArrayInfo** %info.addr, align 8
  %ub = getelementptr inbounds %struct.ArrayInfo, %struct.ArrayInfo* %tmp2, i32 0, i32 8
  %tmp3 = load i64, i64* %ub, align 8
  %call = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str, i64 0, i64 0), i64 %tmp1, i64 %tmp3)
  ret void
}

declare dso_local i32 @printf(i8*, ...) #2

; Function Attrs: noinline nounwind optnone uwtable
define dso_local void @print(%struct.ArrayInfo* %info) #0 {
entry:
  %info.addr = alloca %struct.ArrayInfo*, align 8
  %i = alloca i32, align 4
  store %struct.ArrayInfo* %info, %struct.ArrayInfo** %info.addr, align 8
  %call = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([11 x i8], [11 x i8]* @.str.1, i64 0, i64 0))
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %tmp = load i32, i32* %i, align 4
  %tmp1 = load %struct.ArrayInfo*, %struct.ArrayInfo** %info.addr, align 8
  %size = getelementptr inbounds %struct.ArrayInfo, %struct.ArrayInfo* %tmp1, i32 0, i32 0
  %tmp2 = load i32, i32* %size, align 8
  %cmp = icmp ult i32 %tmp, %tmp2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %tmp3 = load %struct.ArrayInfo*, %struct.ArrayInfo** %info.addr, align 8
  %array = getelementptr inbounds %struct.ArrayInfo, %struct.ArrayInfo* %tmp3, i32 0, i32 6
  %tmp4 = load float*, float** %array, align 8
  %tmp5 = load i32, i32* %i, align 4
  %idxprom = zext i32 %tmp5 to i64
  %arrayidx = getelementptr inbounds float, float* %tmp4, i64 %idxprom
  %tmp6 = load float, float* %arrayidx, align 4
  %conv = fpext float %tmp6 to double
  %call1 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.2, i64 0, i64 0), double %conv)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %tmp7 = load i32, i32* %i, align 4
  %inc = add i32 %tmp7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %tmp8 = load %struct.ArrayInfo*, %struct.ArrayInfo** %info.addr, align 8
  %sum = getelementptr inbounds %struct.ArrayInfo, %struct.ArrayInfo* %tmp8, i32 0, i32 2
  %tmp9 = load double, double* %sum, align 8
  %call2 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([14 x i8], [14 x i8]* @.str.3, i64 0, i64 0), double %tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone uwtable
define dso_local i32 @main() #0 {
entry:
  %retval = alloca i32, align 4
  %n = alloca i32, align 4
  store i32 0, i32* %retval, align 4
  %call = call i32 (i8*, ...) @__isoc99_scanf(i8* getelementptr inbounds ([3 x i8], [3 x i8]* @.str.4, i64 0, i64 0), i32* %n)
  %tmp = load i32, i32* %n, align 4
  store i32 %tmp, i32* getelementptr inbounds (%struct.ArrayInfo, %struct.ArrayInfo* @arrayInfo, i32 0, i32 0), align 8
  call void @allocate(%struct.ArrayInfo* @arrayInfo)
  call void @init(%struct.ArrayInfo* @arrayInfo)
  call void @populate(%struct.ArrayInfo* @arrayInfo)
  call void @print(%struct.ArrayInfo* @arrayInfo)
  ret i32 0
}

declare dso_local i32 @__isoc99_scanf(i8*, ...) #2

; Function Attrs: noinline nounwind optnone uwtable
define dso_local void @init(%struct.ArrayInfo* %info) #0 {
entry:
  %info.addr = alloca %struct.ArrayInfo*, align 8
  %i = alloca i32, align 4
  store %struct.ArrayInfo* %info, %struct.ArrayInfo** %info.addr, align 8
  %tmp = load %struct.ArrayInfo*, %struct.ArrayInfo** %info.addr, align 8
  %isColomnMajor = getelementptr inbounds %struct.ArrayInfo, %struct.ArrayInfo* %tmp, i32 0, i32 1
  store i32 0, i32* %isColomnMajor, align 4
  %tmp1 = load %struct.ArrayInfo*, %struct.ArrayInfo** %info.addr, align 8
  %size = getelementptr inbounds %struct.ArrayInfo, %struct.ArrayInfo* %tmp1, i32 0, i32 0
  %tmp2 = load i32, i32* %size, align 8
  %tmp3 = load %struct.ArrayInfo*, %struct.ArrayInfo** %info.addr, align 8
  %arraySize = getelementptr inbounds %struct.ArrayInfo, %struct.ArrayInfo* %tmp3, i32 0, i32 3
  store i32 %tmp2, i32* %arraySize, align 8
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %tmp4 = load i32, i32* %i, align 4
  %cmp = icmp ult i32 %tmp4, 200
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %tmp5 = load i32, i32* %i, align 4
  %add = add i32 %tmp5, 10
  %tmp6 = load %struct.ArrayInfo*, %struct.ArrayInfo** %info.addr, align 8
  %staticArray = getelementptr inbounds %struct.ArrayInfo, %struct.ArrayInfo* %tmp6, i32 0, i32 4
  %tmp7 = load i32, i32* %i, align 4
  %idxprom = zext i32 %tmp7 to i64
  %arrayidx = getelementptr inbounds [200 x i32], [200 x i32]* %staticArray, i64 0, i64 %idxprom
  store i32 %add, i32* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %tmp8 = load i32, i32* %i, align 4
  %inc = add i32 %tmp8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %tmp9 = load %struct.ArrayInfo*, %struct.ArrayInfo** %info.addr, align 8
  %lb = getelementptr inbounds %struct.ArrayInfo, %struct.ArrayInfo* %tmp9, i32 0, i32 5
  store i64 0, i64* %lb, align 8
  %tmp10 = load %struct.ArrayInfo*, %struct.ArrayInfo** %info.addr, align 8
  %size1 = getelementptr inbounds %struct.ArrayInfo, %struct.ArrayInfo* %tmp10, i32 0, i32 0
  %tmp11 = load i32, i32* %size1, align 8
  %sub = sub nsw i32 %tmp11, 0
  %conv = sext i32 %sub to i64
  %tmp12 = load %struct.ArrayInfo*, %struct.ArrayInfo** %info.addr, align 8
  %ub = getelementptr inbounds %struct.ArrayInfo, %struct.ArrayInfo* %tmp12, i32 0, i32 8
  store i64 %conv, i64* %ub, align 8
  %tmp13 = load %struct.ArrayInfo*, %struct.ArrayInfo** %info.addr, align 8
  %bias = getelementptr inbounds %struct.ArrayInfo, %struct.ArrayInfo* %tmp13, i32 0, i32 9
  store i32 99, i32* %bias, align 8
  %tmp14 = load %struct.ArrayInfo*, %struct.ArrayInfo** %info.addr, align 8
  %sum = getelementptr inbounds %struct.ArrayInfo, %struct.ArrayInfo* %tmp14, i32 0, i32 2
  store double 0.000000e+00, double* %sum, align 8
  ret void
}

; Function Attrs: noinline nounwind optnone uwtable
define dso_local void @populate(%struct.ArrayInfo* %info) #0 {
entry:
  %info.addr = alloca %struct.ArrayInfo*, align 8
  %temp1 = alloca float, align 4
  %temp2 = alloca float, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %k = alloca i32, align 4
  %i28 = alloca i32, align 4
  %i45 = alloca i32, align 4
  %j51 = alloca i32, align 4
  %i97 = alloca i32, align 4
  store %struct.ArrayInfo* %info, %struct.ArrayInfo** %info.addr, align 8
  %tmp = load %struct.ArrayInfo*, %struct.ArrayInfo** %info.addr, align 8
  %bias = getelementptr inbounds %struct.ArrayInfo, %struct.ArrayInfo* %tmp, i32 0, i32 9
  %tmp1 = load i32, i32* %bias, align 8
  %conv = sitofp i32 %tmp1 to float
  store float %conv, float* %temp1, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc25, %entry
  %tmp2 = load i32, i32* %i, align 4
  %tmp3 = load %struct.ArrayInfo*, %struct.ArrayInfo** %info.addr, align 8
  %size = getelementptr inbounds %struct.ArrayInfo, %struct.ArrayInfo* %tmp3, i32 0, i32 0
  %tmp4 = load i32, i32* %size, align 8
  %cmp = icmp ult i32 %tmp2, %tmp4
  br i1 %cmp, label %for.body, label %for.end27

for.body:                                         ; preds = %for.cond
  %tmp5 = load i32, i32* %i, align 4
  %conv2 = uitofp i32 %tmp5 to float
  %tmp6 = load float, float* %temp1, align 4
  %add = fadd float %tmp6, %conv2
  store float %add, float* %temp1, align 4
  store i32 0, i32* %j, align 4
  br label %for.cond3

for.cond3:                                        ; preds = %for.inc22, %for.body
  %tmp7 = load i32, i32* %j, align 4
  %tmp8 = load %struct.ArrayInfo*, %struct.ArrayInfo** %info.addr, align 8
  %size4 = getelementptr inbounds %struct.ArrayInfo, %struct.ArrayInfo* %tmp8, i32 0, i32 0
  %tmp9 = load i32, i32* %size4, align 8
  %cmp5 = icmp ult i32 %tmp7, %tmp9
  br i1 %cmp5, label %for.body7, label %for.end24

for.body7:                                        ; preds = %for.cond3
  %tmp10 = load %struct.ArrayInfo*, %struct.ArrayInfo** %info.addr, align 8
  %staticArray = getelementptr inbounds %struct.ArrayInfo, %struct.ArrayInfo* %tmp10, i32 0, i32 4
  %tmp11 = load i32, i32* %j, align 4
  %rem = urem i32 %tmp11, 200
  %idxprom = zext i32 %rem to i64
  %arrayidx = getelementptr inbounds [200 x i32], [200 x i32]* %staticArray, i64 0, i64 %idxprom
  %tmp12 = load i32, i32* %arrayidx, align 4
  %tmp13 = load i32, i32* %j, align 4
  %add8 = add i32 %tmp12, %tmp13
  %conv9 = uitofp i32 %add8 to float
  store float %conv9, float* %temp2, align 4
  store i32 0, i32* %k, align 4
  br label %for.cond10

for.cond10:                                       ; preds = %for.inc, %for.body7
  %tmp14 = load i32, i32* %k, align 4
  %tmp15 = load %struct.ArrayInfo*, %struct.ArrayInfo** %info.addr, align 8
  %size11 = getelementptr inbounds %struct.ArrayInfo, %struct.ArrayInfo* %tmp15, i32 0, i32 0
  %tmp16 = load i32, i32* %size11, align 8
  %cmp12 = icmp ult i32 %tmp14, %tmp16
  br i1 %cmp12, label %for.body14, label %for.end

for.body14:                                       ; preds = %for.cond10
  %tmp17 = load i32, i32* %i, align 4
  %tmp18 = load i32, i32* %j, align 4
  %add15 = add i32 %tmp17, %tmp18
  %tmp19 = load i32, i32* %k, align 4
  %add16 = add i32 %add15, %tmp19
  %conv17 = uitofp i32 %add16 to float
  %tmp20 = load float, float* %temp1, align 4
  %add18 = fadd float %conv17, %tmp20
  %tmp21 = load float, float* %temp2, align 4
  %add19 = fadd float %add18, %tmp21
  %tmp22 = load %struct.ArrayInfo*, %struct.ArrayInfo** %info.addr, align 8
  %array = getelementptr inbounds %struct.ArrayInfo, %struct.ArrayInfo* %tmp22, i32 0, i32 6
  %tmp23 = load float*, float** %array, align 8
  %tmp24 = load i32, i32* %k, align 4
  %idxprom20 = zext i32 %tmp24 to i64
  %arrayidx21 = getelementptr inbounds float, float* %tmp23, i64 %idxprom20
  store float %add19, float* %arrayidx21, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body14
  %tmp25 = load i32, i32* %k, align 4
  %inc = add i32 %tmp25, 1
  store i32 %inc, i32* %k, align 4
  br label %for.cond10

for.end:                                          ; preds = %for.cond10
  br label %for.inc22

for.inc22:                                        ; preds = %for.end
  %tmp26 = load i32, i32* %j, align 4
  %inc23 = add i32 %tmp26, 1
  store i32 %inc23, i32* %j, align 4
  br label %for.cond3

for.end24:                                        ; preds = %for.cond3
  br label %for.inc25

for.inc25:                                        ; preds = %for.end24
  %tmp27 = load i32, i32* %i, align 4
  %inc26 = add i32 %tmp27, 1
  store i32 %inc26, i32* %i, align 4
  br label %for.cond

for.end27:                                        ; preds = %for.cond
  store i32 0, i32* %i28, align 4
  br label %for.cond29

for.cond29:                                       ; preds = %for.inc39, %for.end27
  %tmp28 = load i32, i32* %i28, align 4
  %tmp29 = load %struct.ArrayInfo*, %struct.ArrayInfo** %info.addr, align 8
  %size30 = getelementptr inbounds %struct.ArrayInfo, %struct.ArrayInfo* %tmp29, i32 0, i32 0
  %tmp30 = load i32, i32* %size30, align 8
  %cmp31 = icmp ult i32 %tmp28, %tmp30
  br i1 %cmp31, label %for.body33, label %for.end41

for.body33:                                       ; preds = %for.cond29
  %tmp31 = load %struct.ArrayInfo*, %struct.ArrayInfo** %info.addr, align 8
  %array34 = getelementptr inbounds %struct.ArrayInfo, %struct.ArrayInfo* %tmp31, i32 0, i32 6
  %tmp32 = load float*, float** %array34, align 8
  %tmp33 = load i32, i32* %i28, align 4
  %idxprom35 = zext i32 %tmp33 to i64
  %arrayidx36 = getelementptr inbounds float, float* %tmp32, i64 %idxprom35
  %tmp34 = load float, float* %arrayidx36, align 4
  %conv37 = fpext float %tmp34 to double
  %tmp35 = load %struct.ArrayInfo*, %struct.ArrayInfo** %info.addr, align 8
  %sum = getelementptr inbounds %struct.ArrayInfo, %struct.ArrayInfo* %tmp35, i32 0, i32 2
  %tmp36 = load double, double* %sum, align 8
  %add38 = fadd double %tmp36, %conv37
  store double %add38, double* %sum, align 8
  br label %for.inc39

for.inc39:                                        ; preds = %for.body33
  %tmp37 = load i32, i32* %i28, align 4
  %inc40 = add i32 %tmp37, 1
  store i32 %inc40, i32* %i28, align 4
  br label %for.cond29

for.end41:                                        ; preds = %for.cond29
  %tmp38 = load %struct.ArrayInfo*, %struct.ArrayInfo** %info.addr, align 8
  %sum42 = getelementptr inbounds %struct.ArrayInfo, %struct.ArrayInfo* %tmp38, i32 0, i32 2
  %tmp39 = load double, double* %sum42, align 8
  %cmp43 = fcmp olt double %tmp39, 5.000000e+03
  br i1 %cmp43, label %if.then, label %if.end92

if.then:                                          ; preds = %for.end41
  store i32 0, i32* %i45, align 4
  br label %for.cond46

for.cond46:                                       ; preds = %for.inc89, %if.then
  %tmp40 = load i32, i32* %i45, align 4
  %tmp41 = load %struct.ArrayInfo*, %struct.ArrayInfo** %info.addr, align 8
  %size47 = getelementptr inbounds %struct.ArrayInfo, %struct.ArrayInfo* %tmp41, i32 0, i32 0
  %tmp42 = load i32, i32* %size47, align 8
  %cmp48 = icmp ult i32 %tmp40, %tmp42
  br i1 %cmp48, label %for.body50, label %for.end91

for.body50:                                       ; preds = %for.cond46
  store i32 0, i32* %j51, align 4
  br label %for.cond52

for.cond52:                                       ; preds = %for.inc86, %for.body50
  %tmp43 = load i32, i32* %j51, align 4
  %tmp44 = load %struct.ArrayInfo*, %struct.ArrayInfo** %info.addr, align 8
  %size53 = getelementptr inbounds %struct.ArrayInfo, %struct.ArrayInfo* %tmp44, i32 0, i32 0
  %tmp45 = load i32, i32* %size53, align 8
  %cmp54 = icmp ult i32 %tmp43, %tmp45
  br i1 %cmp54, label %for.body56, label %for.end88

for.body56:                                       ; preds = %for.cond52
  %tmp46 = load %struct.ArrayInfo*, %struct.ArrayInfo** %info.addr, align 8
  %sum57 = getelementptr inbounds %struct.ArrayInfo, %struct.ArrayInfo* %tmp46, i32 0, i32 2
  %tmp47 = load double, double* %sum57, align 8
  %cmp58 = fcmp olt double %tmp47, 2.500000e+03
  br i1 %cmp58, label %if.then60, label %if.end

if.then60:                                        ; preds = %for.body56
  %tmp48 = load i32, i32* %i45, align 4
  %tmp49 = load i32, i32* %j51, align 4
  %add61 = add i32 %tmp48, %tmp49
  %conv62 = uitofp i32 %add61 to double
  %tmp50 = load %struct.ArrayInfo*, %struct.ArrayInfo** %info.addr, align 8
  %staticArray63 = getelementptr inbounds %struct.ArrayInfo, %struct.ArrayInfo* %tmp50, i32 0, i32 4
  %tmp51 = load i32, i32* %j51, align 4
  %rem64 = urem i32 %tmp51, 200
  %idxprom65 = zext i32 %rem64 to i64
  %arrayidx66 = getelementptr inbounds [200 x i32], [200 x i32]* %staticArray63, i64 0, i64 %idxprom65
  %tmp52 = load i32, i32* %arrayidx66, align 4
  %conv67 = sitofp i32 %tmp52 to double
  %call = call double @pow(double %conv67, double 2.000000e+00) #3
  %add68 = fadd double %conv62, %call
  %conv69 = fptrunc double %add68 to float
  %tmp53 = load %struct.ArrayInfo*, %struct.ArrayInfo** %info.addr, align 8
  %array70 = getelementptr inbounds %struct.ArrayInfo, %struct.ArrayInfo* %tmp53, i32 0, i32 6
  %tmp54 = load float*, float** %array70, align 8
  %tmp55 = load i32, i32* %j51, align 4
  %idxprom71 = zext i32 %tmp55 to i64
  %arrayidx72 = getelementptr inbounds float, float* %tmp54, i64 %idxprom71
  store float %conv69, float* %arrayidx72, align 4
  br label %for.inc86

if.end:                                           ; preds = %for.body56
  %tmp56 = load i32, i32* %i45, align 4
  %tmp57 = load i32, i32* %j51, align 4
  %add73 = add i32 %tmp56, %tmp57
  %conv74 = uitofp i32 %add73 to double
  %tmp58 = load %struct.ArrayInfo*, %struct.ArrayInfo** %info.addr, align 8
  %staticArray75 = getelementptr inbounds %struct.ArrayInfo, %struct.ArrayInfo* %tmp58, i32 0, i32 4
  %tmp59 = load i32, i32* %j51, align 4
  %rem76 = urem i32 %tmp59, 200
  %idxprom77 = zext i32 %rem76 to i64
  %arrayidx78 = getelementptr inbounds [200 x i32], [200 x i32]* %staticArray75, i64 0, i64 %idxprom77
  %tmp60 = load i32, i32* %arrayidx78, align 4
  %conv79 = sitofp i32 %tmp60 to double
  %call80 = call double @pow(double %conv79, double 1.000000e+00) #3
  %add81 = fadd double %conv74, %call80
  %conv82 = fptrunc double %add81 to float
  %tmp61 = load %struct.ArrayInfo*, %struct.ArrayInfo** %info.addr, align 8
  %array83 = getelementptr inbounds %struct.ArrayInfo, %struct.ArrayInfo* %tmp61, i32 0, i32 6
  %tmp62 = load float*, float** %array83, align 8
  %tmp63 = load i32, i32* %j51, align 4
  %idxprom84 = zext i32 %tmp63 to i64
  %arrayidx85 = getelementptr inbounds float, float* %tmp62, i64 %idxprom84
  store float %conv82, float* %arrayidx85, align 4
  br label %for.inc86

for.inc86:                                        ; preds = %if.end, %if.then60
  %tmp64 = load i32, i32* %j51, align 4
  %inc87 = add i32 %tmp64, 1
  store i32 %inc87, i32* %j51, align 4
  br label %for.cond52

for.end88:                                        ; preds = %for.cond52
  br label %for.inc89

for.inc89:                                        ; preds = %for.end88
  %tmp65 = load i32, i32* %i45, align 4
  %inc90 = add i32 %tmp65, 1
  store i32 %inc90, i32* %i45, align 4
  br label %for.cond46

for.end91:                                        ; preds = %for.cond46
  br label %if.end92

if.end92:                                         ; preds = %for.end91, %for.end41
  %tmp66 = load %struct.ArrayInfo*, %struct.ArrayInfo** %info.addr, align 8
  %bias93 = getelementptr inbounds %struct.ArrayInfo, %struct.ArrayInfo* %tmp66, i32 0, i32 9
  %tmp67 = load i32, i32* %bias93, align 8
  %cmp94 = icmp slt i32 %tmp67, 100
  br i1 %cmp94, label %if.then96, label %if.end112

if.then96:                                        ; preds = %if.end92
  store i32 0, i32* %i97, align 4
  br label %for.cond98

for.cond98:                                       ; preds = %for.inc109, %if.then96
  %tmp68 = load i32, i32* %i97, align 4
  %tmp69 = load %struct.ArrayInfo*, %struct.ArrayInfo** %info.addr, align 8
  %size99 = getelementptr inbounds %struct.ArrayInfo, %struct.ArrayInfo* %tmp69, i32 0, i32 0
  %tmp70 = load i32, i32* %size99, align 8
  %cmp100 = icmp ult i32 %tmp68, %tmp70
  br i1 %cmp100, label %for.body102, label %for.end111

for.body102:                                      ; preds = %for.cond98
  %tmp71 = load %struct.ArrayInfo*, %struct.ArrayInfo** %info.addr, align 8
  %array103 = getelementptr inbounds %struct.ArrayInfo, %struct.ArrayInfo* %tmp71, i32 0, i32 6
  %tmp72 = load float*, float** %array103, align 8
  %tmp73 = load i32, i32* %i97, align 4
  %idxprom104 = zext i32 %tmp73 to i64
  %arrayidx105 = getelementptr inbounds float, float* %tmp72, i64 %idxprom104
  %tmp74 = load float, float* %arrayidx105, align 4
  %conv106 = fpext float %tmp74 to double
  %tmp75 = load %struct.ArrayInfo*, %struct.ArrayInfo** %info.addr, align 8
  %sum107 = getelementptr inbounds %struct.ArrayInfo, %struct.ArrayInfo* %tmp75, i32 0, i32 2
  %tmp76 = load double, double* %sum107, align 8
  %add108 = fadd double %tmp76, %conv106
  store double %add108, double* %sum107, align 8
  br label %for.inc109

for.inc109:                                       ; preds = %for.body102
  %tmp77 = load i32, i32* %i97, align 4
  %inc110 = add i32 %tmp77, 1
  store i32 %inc110, i32* %i97, align 4
  br label %for.cond98

for.end111:                                       ; preds = %for.cond98
  br label %if.end112

if.end112:                                        ; preds = %for.end111, %if.end92
  ret void
}

; Function Attrs: nounwind
declare dso_local double @pow(double, double) #1

attributes #0 = { noinline nounwind optnone uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="all" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="all" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="all" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }

!llvm.ident = !{!0, !0}
!llvm.module.flags = !{!1}

!0 = !{!"clang version 11.0.0 (https://github.com/llvm/llvm-project.git 47ef09e4848a970c530928496b54085cfdba5a76)"}
!1 = !{i32 1, !"wchar_size", i32 4}
