; ModuleID = '3new.ll'
source_filename = "3new.c"
target datalayout = "e-m:e-p270:32:32-p271:32:32-p272:64:64-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@gSig = global i32 0

; Function Attrs: noinline nounwind optnone uwtable
define dso_local void @small(i32 %b) #0 {
entry:
  %b.addr = alloca i32, align 4
  %a = alloca i32, align 4
  store i32 %b, i32* %b.addr, align 4
  store i32 0, i32* %a, align 4
  %0 = load i32, i32* %b.addr, align 4
  %tobool = icmp ne i32 %0, 0
  store i32 0, i32* @gSig, align 4
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load i32, i32* @gSig, align 4
  %2 = xor i32 %1, 1
  %3 = icmp ne i32 %2, 1
  store i32 10, i32* %a, align 4
  store i32 %2, i32* @gSig, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %4 = load i32, i32* @gSig, align 4
  %5 = xor i32 %4, 2
  %6 = icmp ne i32 %5, 2
  store i32 20, i32* %a, align 4
  store i32 %5, i32* @gSig, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %D = phi i32 [ 0, %if.else ], [ 3, %if.then ]
  %7 = load i32, i32* @gSig, align 4
  %8 = xor i32 %7, 1
  %9 = xor i32 %8, %D
  %10 = icmp ne i32 %9, 3
  store i32 %9, i32* @gSig, align 4
  ret void
}

attributes #0 = { noinline nounwind optnone uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="all" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (https://github.com/llvm/llvm-project.git 47ef09e4848a970c530928496b54085cfdba5a76)"}
