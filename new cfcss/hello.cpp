#include "llvm/ADT/IndexedMap.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/Analysis/LoopPass.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/GlobalVariable.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Type.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Transforms/IPO/PassManagerBuilder.h"

using namespace llvm;

namespace
{
    struct hello : public FunctionPass
    {
        static char ID;
        hello() : FunctionPass(ID) {}
        static int count;
        bool runOnFunction(Function &F);

    private:
        std::map<BasicBlock *, int> BBToSig;
        void SignatureDifference(BasicBlock *bb);
        void handleBranchFanIn(BasicBlock *bb);
        GlobalVariable *gSig;
    };
} // namespace

int hello::count = 0;

bool hello::runOnFunction(Function &F)
{
    Module *M = F.getParent();
    IRBuilder<> Builder(M->getContext());
    if (!M->getNamedValue("gSig"))
    {
        gSig = new GlobalVariable(*M, IntegerType::get(F.getContext(), 32), false,
                                  GlobalValue::ExternalLinkage, 0, "gSig");
        ConstantInt *zero = Builder.getInt32(0);
        gSig->setInitializer(zero);
    }
    for (auto bb = F.begin(); bb != F.end(); bb++)
    {
        BBToSig[&*bb] = count++;
    }
    for (std::map<BasicBlock *, int>::iterator iter = BBToSig.begin();
         iter != BBToSig.end(); iter++)
    {
        SignatureDifference(iter->first);
    }
}
void hello::SignatureDifference(BasicBlock *bb)
{
    Instruction *InsertPt = (Instruction *)(bb->getFirstNonPHI());
    IRBuilder<> Builder(InsertPt);
    int firstBBSig = 0;
    Value *constSig1;
    Value *xr;
    LLVMContext &context = bb->getParent()->getContext();

    if (!pred_size(bb))
    {
        int currSig = BBToSig[bb];
        Type *uintTp = Type::getInt32Ty(context);
        Value *constSig = ConstantInt::get(uintTp, currSig);
        Builder.SetInsertPoint(bb->getTerminator());
        Builder.CreateStore(constSig, gSig);
        return;
    }

    if (bb->getUniquePredecessor() == NULL)
    {
        PHINode *PN = Builder.CreatePHI(Type::getInt32Ty(context), 0, "D");

        for (auto PI = pred_begin(bb); PI != pred_end(bb); ++PI)
        {
            BasicBlock *predecessorBB = *PI;
            Instruction *InsertPt;
            LLVMContext &context = predecessorBB->getParent()->getContext();

            InsertPt = (Instruction *)(predecessorBB->getFirstNonPHI());

            if (PI == pred_begin(bb))
            {
                firstBBSig = BBToSig[predecessorBB];

                constSig1 = ConstantInt::get(Type::getInt32Ty(context), firstBBSig);
                xr = Builder.CreateXor(constSig1, constSig1, "D");
            }
            else
            {
                int currSig = BBToSig[predecessorBB];

                Value *constSig2 = ConstantInt::get(Type::getInt32Ty(context), currSig);
                xr = Builder.CreateXor(constSig1, constSig2, "D");
            }
            PN->addIncoming(xr, predecessorBB);
        }

        int currSig = BBToSig[bb];
        int sigDiff = firstBBSig ^ currSig;
        LoadInst *G = Builder.CreateLoad(gSig);
        Value *xorInst = Builder.CreateXor(G, sigDiff);
        Value *xorInst_D = Builder.CreateXor(xorInst, PN);

        Value *currSigConst = ConstantInt::get(xorInst_D->getType(), currSig);
        Value *cmpInst = Builder.CreateICmpNE(xorInst_D, currSigConst);

        Builder.SetInsertPoint(bb->getTerminator());
        Builder.CreateStore(xorInst_D, gSig);
        return;
    }

    BasicBlock *predecessorBB = bb->getUniquePredecessor();

    int predSig = BBToSig[predecessorBB];
    int currSig = BBToSig[bb];
    int sigDiff = predSig ^ currSig;

    LoadInst *G = Builder.CreateLoad(gSig);

    Value *xorInst = Builder.CreateXor(G, sigDiff);

    Value *currSigConst = ConstantInt::get(xorInst->getType(), currSig);
    Value *cmpInst = Builder.CreateICmpNE(xorInst, currSigConst);

    Builder.SetInsertPoint(bb->getTerminator());
    Builder.CreateStore(xorInst, gSig);
}

char hello::ID = 0;
static RegisterPass<hello> X("hello", "hello World Pass",
                             false /* Only looks at CFG */,
                             false /* Analysis Pass */);

static RegisterStandardPasses Y(PassManagerBuilder::EP_EarlyAsPossible,
                                [](const PassManagerBuilder &Builder,
                                   legacy::PassManagerBase &PM) {
                                    PM.add(new hello());
                                });
