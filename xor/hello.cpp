#include "llvm/Pass.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Instructions.h"
#include "llvm/Support/raw_ostream.h"

#include "llvm/Analysis/CallGraph.h"
#include "llvm/Analysis/CallGraphSCCPass.h"

#include "llvm/IR/LegacyPassManager.h"
#include "llvm/Transforms/IPO/PassManagerBuilder.h"

using namespace llvm;

namespace
{
struct hello : public ModulePass
{
  static char ID;
  hello() : ModulePass(ID) {}

  bool runOnModule(Module &M);
  GlobalVariable *gvar_ptr_abc;
  Instruction *NewInst;

  ConstantInt *const_int_5;
  ConstantInt *variable;
  Value *bbID;
  Value *glob;
  Value *xorValue;
  StoreInst *store_5;
  ConstantInt *xorval;
};
} // namespace

bool hello::runOnModule(Module &M)
{
  bool modified = false; // = initialize(M);
  int count;
  count = 0;
  IRBuilder<> Builder(M.getContext());
  hello::gvar_ptr_abc = new GlobalVariable(/*Module=*/M,
                                           /*Type=*/Builder.getInt32Ty(),
                                           /*isConstant=*/false,
                                           /*Linkage=*/GlobalValue::ExternalLinkage,
                                           /*Initializer=*/0, // has initializer, specified below
                                           /*Name=*/"abc");

  std::map<llvm::BasicBlock *, llvm::ConstantInt *> func_bb_map;

  for (auto it = M.begin(); it != M.end(); it++)
  {

    for (auto bb = it->begin(); bb != it->end(); bb++)
    {
      if (func_bb_map.find(&*bb) == func_bb_map.end())
      {
        count++;
        func_bb_map[&*bb] = Builder.getInt32(count);
      }
    }
  }

  for (auto it = M.begin(); it != M.end(); it++)
  {

    for (auto bb = it->begin(); bb != it->end(); bb++)
    {
      auto I = bb->begin();
      if (bb->getName() != "entry" && bb->getName() != "return")
      {

        llvm::LoadInst *loadInstruct = new LoadInst(/*Type *Ty*/ llvm::Type::getInt32Ty(M.getContext()),
                                                    /*Value *Ptr*/ gvar_ptr_abc,
                                                    /*const Twine &NameStr=""*/ "globValLoad",
                                                    /*Instruction *InsertBefore=nullptr*/ &*I);

        llvm::Instruction *newInst = BinaryOperator::Create(llvm::Instruction::Xor,
                                                            loadInstruct,
                                                            func_bb_map[&*bb],
                                                            "XORval",
                                                            &*I);

        hello::store_5 = new StoreInst(newInst, gvar_ptr_abc, &*I);
      }
    }
  }

  return false;
}

char hello::ID = 0;

//Register as following to run through opt tool
static RegisterPass<hello>
    X("hello", "Global Variable XOR BB ID", false, false);

// Registration to run by default using clang compiler
static void registerMyPass(const PassManagerBuilder &, legacy::PassManagerBase &PM)
{
  PM.add(new hello());
}

static RegisterStandardPasses RegisterMyPass1(PassManagerBuilder::EP_ModuleOptimizerEarly, registerMyPass);