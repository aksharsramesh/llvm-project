#include "llvm/ADT/Statistic.h"
#include "llvm/Analysis/LoopPass.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/GlobalVariable.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Type.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Transforms/IPO/PassManagerBuilder.h"

using namespace llvm;

namespace
{
    struct hello : public FunctionPass
    {
        static char ID;
        hello() : FunctionPass(ID) {}
        static int count;
        bool runOnFunction(Function &F);

    private:
        std::map<BasicBlock *, int> BBToSig;
        std::map<BasicBlock *, int> visited;
        void AddCFCSSinst(BasicBlock *bb);
        static GlobalVariable *gSig;
    };
} // namespace

int hello::count = 0;
GlobalVariable *hello::gSig = NULL;

bool hello::runOnFunction(Function &F)
{
    Module *M = F.getParent();
    if (!gSig)
    {
        gSig = new GlobalVariable(*M, IntegerType::get(F.getContext(), 32), false,
                                  GlobalValue::ExternalLinkage, 0, "gSig");
        ConstantInt *zero =
            ConstantInt::get(F.getContext(), APInt(32, StringRef("0"), 10));
        gSig->setInitializer(zero);
    }
    for (auto bb = F.begin(); bb != F.end(); bb++)
    {
        BBToSig[&*bb] = count++;
    }
    for (std::map<BasicBlock *, int>::iterator iter = BBToSig.begin();
         iter != BBToSig.end(); iter++)
    {
        AddCFCSSinst(iter->first);
    }
}
void hello::AddCFCSSinst(BasicBlock *bb)
{
    Instruction *InsertPt = (Instruction *)(bb->getFirstNonPHI());
    IRBuilder<> Builder(InsertPt);
    LLVMContext &context = bb->getParent()->getContext();

    if (visited.find(bb) == visited.end())
    {
        if (pred_begin(bb) == pred_end(bb))
        {
            int currSig = BBToSig[bb];
            Value *constSig = ConstantInt::get(Type::getInt32Ty(context), currSig);
            Builder.SetInsertPoint(bb->getTerminator());
            Builder.CreateStore(constSig, gSig);
            visited[bb] = 1;
            return;
        }

        if (bb->getUniquePredecessor() == NULL)
        {
            errs() << bb->getName() << " Branch Fan In Case\n";
        }
        else
        {
            BasicBlock *predecessorBB = bb->getUniquePredecessor();

            int predSig = BBToSig[predecessorBB];
            int currSig = BBToSig[bb];
            int sigDiff = predSig ^ currSig;

            LoadInst *G = Builder.CreateLoad(gSig);

            Value *xorInst = Builder.CreateXor(G, sigDiff);

            Value *currSigConst = ConstantInt::get(xorInst->getType(), currSig);
            Value *cmpInst = Builder.CreateICmpNE(xorInst, currSigConst);

            Builder.SetInsertPoint(bb->getTerminator());
            Builder.CreateStore(xorInst, gSig);
        }
        visited[bb] = 1;
    }
}

char hello::ID = 0;
static RegisterPass<hello> X("hello", "Hello World Pass",
                             false /* Only looks at CFG */,
                             false /* Analysis Pass */);

static RegisterStandardPasses Y(PassManagerBuilder::EP_EarlyAsPossible,
                                [](const PassManagerBuilder &Builder,
                                   legacy::PassManagerBase &PM) {
                                    PM.add(new hello());
                                });
